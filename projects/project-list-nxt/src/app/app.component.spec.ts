import {AppComponent} from './app.component';

describe('AppComponent', () => {
  it('should be truthy', () => {
    const comp = new AppComponent();
    expect(comp).toBeTruthy();
  });
});
