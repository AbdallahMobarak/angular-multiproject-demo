import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lib-header',
  template: `
    <mat-toolbar>
      <span>{{title}}</span>
    </mat-toolbar>
  `,
  styles: []
})
export class HeaderComponent implements OnInit {
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

}
