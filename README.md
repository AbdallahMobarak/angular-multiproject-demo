# ProjectList

This projects demonstrates the use of the multi-project-builds with @angular/cli.

## Development server

After installing the dependencies with `npm install`, run `ng build library --watch` to build the library and watch for changes. Run `npm run start` in a different terminal to start both Angular applications in the devleopment mode. Navigate to `http://localhost:4200` and `http://localhost:4201` to test the applications.

## Build

Run `npm run build` to build the library and both projects.